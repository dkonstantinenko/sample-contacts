class Contact < ActiveRecord::Base
  attr_accessible :address, :email, :first_name, :second_name, :site, :telephone
end
