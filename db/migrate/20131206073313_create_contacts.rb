class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :first_name
      t.string :second_name
      t.string :telephone
      t.string :email
      t.string :site
      t.string :address

      t.timestamps
    end
  end
end
